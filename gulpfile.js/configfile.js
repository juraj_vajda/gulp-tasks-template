'use strict';

var gutil       = require('gulp-util');

/**
 * MAIN CONFIG
 * @description:
 * !! turn undefined into a proper false
 * --production flag: used for minifying the files
 * --watch flag: used for watch tasks
 */

var config = {
  production: !!gutil.env.production,
  watch: !!gutil.env.watch,
  external: !!gutil.env.external
};

module.exports = config;
