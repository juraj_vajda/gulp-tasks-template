'use strict';

var gulp        = require('gulp');
var paths       = require('../../package.json').paths;
var sassdoc     = require('sassdoc');
var config      = require('../configfile.js');

/**
 * SASSDOC - SASS Documentation and Guidelines
 */

// use external path defined in package.json when --external flag is given
var sassdoc_dest = config.external ? paths.sassdoc.dest_ext : paths.sassdoc.dest;
var sass_src = config.external ? paths.sass.src_ext : paths.sass.src;

gulp.task('sassdoc', function(){
  var options = {
    dest: './'+sassdoc_dest,
    verbose: true,
    debug: false
  };

  return gulp.src(sass_src+'/**/*.scss')
  .pipe(sassdoc(options));
});
