'use strict';

var gulp 	 		 = require('gulp');
var sass 	 		 = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var paths  		 = require('../../package.json').paths;
var config 		 = require('../configfile.js');

// use external path defined in package.json when --external flag is given
var sass_src = config.external ? paths.sass.src_ext : paths.sass.src;
var sass_dest = config.external ? paths.sass.dest_ext : paths.sass.dest;

gulp.task('sass-compile', function () {
  return gulp.src(sass_src + '**/*.scss')
  	.pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sass()
    .on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(sass_dest));
});
