'use strict';

var browserify  = require('browserify');
var gulp        = require('gulp');
var sourcemaps  = require('gulp-sourcemaps');
var gutil       = require('gulp-util');
var source      = require('vinyl-source-stream');
var paths       = require('../../package.json').paths;
var config      = require('../configfile.js');
var buffer      = require('vinyl-buffer');
var watchify    = require('watchify');
var xtend       = require('xtend');
var tsify       = require('tsify');

// use external path defined in package.json when --external flag is given
var ts_src = config.external ? paths.ts.src_ext : paths.ts.src;
var ts_dest = config.external ? paths.ts.dest_ext : paths.ts.dest;

/**
 * BROWSERIFY - JavaScript
 */
gulp.task('js-compile', function(){
  var opts = {
    entries: [ts_src + 'main.ts'], // browserify requires relative path
    debug: false,
    plugin: [tsify]
  };

  if (config.watch){
    opts = xtend(opts, watchify.args);
  }

  var bundler = browserify(opts);

  if (config.watch){
    bundler = watchify(bundler);
  }
  // optionally transform
  bundler.transform('babelify', {
      presets: ['es2015'],
      extensions: ['.ts']
    });

  bundler.on('update', function(ids){
    gutil.log('File(s) changed: ' + gutil.colors.cyan(ids));
    gutil.log('Rebundling...');
    rebundle();
  });

  bundler.on('log', gutil.log);

  function rebundle(){
    return bundler.bundle()
      .on('error', function(e){
        gutil.log('Browserify Error', gutil.colors.red(e));
      })
      .pipe(source('bundle.js'))
      // sourcemaps
      .pipe(buffer())
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(sourcemaps.write('./'))
      //
      .pipe(gulp.dest(ts_dest));
  }

  return rebundle();
});
