'use strict';

var gulp    = require('gulp');
var paths   = require('../../package.json').paths;
var eslint  = require('gulp-eslint');
var config  = require('../configfile.js');

// use external path defined in package.json when --external flag is given
var ts_src = config.external ? paths.ts.src_ext : paths.ts.src;

/**
 * ESLINT
 * @description: ESLint ignores files with 'node_modules' paths. 
 * So, it's best to have gulp ignore the directory as well. 
 * Also, Be sure to return the stream from the task; 
 * Otherwise, the task may end before the stream has finished.
 */

gulp.task('eslint', ['js-compile'], function(){
  var stream = gulp.src(ts_src + '**/*.ts')
    // eslint() attaches the lint output to the 'eslint' property 
    // of the file object so it can be used by other modules. 
    .pipe(eslint({
      configFile: './package.json',
      useEslintrc: false
    }))
    // eslint.format() outputs the lint results to the console. 
    // Alternatively use eslint.formatEach() (see Docs). 
    .pipe(eslint.format())
    // To have the process exit with an error code (1) on 
    // lint error, return the stream and pipe to failAfterError last. 
    .pipe(eslint.failAfterError());

  return stream;
});
