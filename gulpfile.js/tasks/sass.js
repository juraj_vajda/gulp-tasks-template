'use strict';

var gulp    = require('gulp');
var paths   = require('../../package.json').paths;
var config  = require('../configfile.js');

// use external path defined in package.json when --external flag is given
var sass_src = config.external ? paths.sass.src_ext : paths.sass.src;

if (config.production) {
  // production sass tasks (minify css at the end)
  gulp.task('sass', ['css-minify']);

} else if (config.watch) {
  // watch task for sass
  gulp.task('sass', function(){
    gulp.watch(sass_src  + '**/*.scss', ['sass-compile']);
  });

} else {
  // default sass task
  gulp.task('sass', ['sass-compile']);
}
