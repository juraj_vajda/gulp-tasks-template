'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');

/**
 * BUILD
 * @description: build javascript, compass and hologram. For production use --production parameter.
 */
 
gulp.task('build', function(cb){
  runSequence('js', 'sass', 'html', cb);
});
