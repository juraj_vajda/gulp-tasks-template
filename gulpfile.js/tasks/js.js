'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');
var config      = require('../configfile.js');

if (config.production) {
  // production js task with compressing
  gulp.task('js', function(cb){
    runSequence('js-compile', 'js-compress',cb);
  });

} else {
  // default js task
  /**
   * @README
   * Eslint will run AFTER the compilation in order to fix the errors.
   * After all errors are fixed we can add Eslint BEFORE the compilations.
   * DISABLED
   */
  // gulp.task('js', ['eslint']);

  // run js compilation without eslint
  gulp.task('js', ['js-compile']);
}
