'use strict';

var gulp   = require('gulp');
var paths  = require('../../package.json').paths;
var config = require('../configfile.js');

// use external path defined in package.json when --external flag is given
var html_src = config.external ? paths.html.src_ext : paths.html.src;
var html_dest = config.external ? paths.html.dest_ext : paths.html.dest;

gulp.task('html-copy', function() {
  return gulp.src(html_src + '*.html')
    .pipe(gulp.dest(html_dest));
});
