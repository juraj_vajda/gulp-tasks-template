'use strict';

var gulp    = require('gulp');
var gutil   = require('gulp-util');

/**
 * @type: array
 * @description : list of all task objects including descriptions to render out as a help in console/terminal, include only main, useful tasks what should be used in terminal not a helpers or side tasks
 */
var tasksArr = [
  {
    name : 'sass',
    command: 'gulp sass [--watch, --production]',
    description: 'compiles the sass files from main project with source maps, use "--production" flag for minifying the output'
  },
  {
    name : 'js',
    command: 'gulp js [--watch, --production]',
    description: 'compiles TypeScript files from main project and includes source maps, use "--production" flag for uglifying the output'
  },
  {
    name : 'html',
    command: 'gulp html [--watch, --production]',
    description: 'copy HTML pages to distribution folder'
  },
  {
    name : 'build',
    command: 'gulp build [--watch, --production]',
    description: 'build task for TypeScript and SASS files, will build all SASS and TS files, also copies the HTML pages to destination folder'
  },
  {
    name : 'SassDoc',
    command: 'gulp sassdoc',
    description: 'a documentation system to build pretty and powerful docs in the blink of an eye (for sass only)'
  },
  {
    name : 'jsDoc',
    command: 'gulp jsdoc',
    description: 'JSDoc 3 is an API documentation generator for JavaScript, similar to Javadoc or phpDocumentor. You add documentation comments directly to your source code, right alongside the code itself. The JSDoc tool will scan your source code and generate an HTML documentation website for you.'
  },
  {
    name : 'TypeDoc',
    command: 'gulp typedoc',
    description: 'A documentation generator for TypeScript projects. Generates TypeScript documentation.'
  },
  {
    name : 'ESLint',
    command: 'gulp eslint',
    description: 'ESLint is an open source project originally created by Nicholas C. Zakas in June 2013. Its goal is to provide a pluggable linting utility for JavaScript and JSX, uses babel-eslint plugin'
  }
];

/**
 * @type: array
 * @description : list of all flags including descriptions to render out as a help in console/terminal
 */

 var flagsArr = [
  {
    name : 'production flag',
    command: '--production',
    description: 'add this parameter to the gulp task for production purposes, mostly for minifying, compressing and concatenating files'
  },
  {
    name : 'watch flag',
    command: '--watch',
    description: 'add this parameter to the gulp task for watching file changes and compiling on the fly'
  },
  {
    name : 'external flag',
    command: '--external',
    description: 'add this parameter to the gulp task if you want to use "external"/secondary sources or destinations'
  }
 ];

// help and list of main tasks [default]
gulp.task('default', function(){
  console.log('\nAVAILABLE FLAGS');
  console.log('=================================\n');

  for (var i = 0; i < flagsArr.length; i++) {
    flagsArr[i]
    console.log(gutil.colors.cyan(flagsArr[i].command)+' : '+flagsArr[i].name+', '+flagsArr[i].description+'\n');
  }

  console.log('=================================\n');

  console.log('\nAVAILABLE TASKS');
  console.log('=================================\n');

  for (var j = 0; j < tasksArr.length; j++) {
    tasksArr[i]
    console.log(gutil.colors.cyan(tasksArr[j].command)+' : '+tasksArr[j].name+', '+tasksArr[j].description+'\n');
  }

  console.log('=================================\n');
});
