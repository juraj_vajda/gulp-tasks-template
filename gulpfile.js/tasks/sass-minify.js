'use strict';

var gulp        = require('gulp');
var cleanCSS    = require('gulp-clean-css');
var paths       = require('../../package.json').paths;
var config      = require('../configfile.js');

// use external path defined in package.json when --external flag is given
var sass_dest = config.external ? paths.sass.dest_ext : paths.sass.dest;

// minify CSS after 'mmq' task, mmq tends to prettify the CSS even though compass minified it before
gulp.task('css-minify', ['sass-compile'], function(){
    var stream = gulp.src(sass_dest + 'main.css')
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log('=================================');
            console.log('file name : '+details.name );
            console.log('original size : ' + details.stats.originalSize/1000+'kb');
            console.log('minified size : ' + details.stats.minifiedSize/1000+'kb');
            console.log('efficiency : ' + Math.round(details.stats.efficiency*100)+'%');
            console.log('=================================');
        }))
        .pipe(gulp.dest(sass_dest));
    return stream;
});
