'use strict';

var gulp    = require('gulp');
var config  = require('../configfile.js');
var paths   = require('../../package.json').paths;
var typedoc = require('gulp-typedoc');

// use external path defined in package.json when --external flag is given
var ts_src = config.external ? paths.ts.src_ext : paths.ts.src;
var typedoc_dest = config.external ? paths.typedoc.dest_ext : paths.typedoc.dest;

gulp.task('typedoc', function() {
  return gulp
  .src([ts_src + '**/*.ts'])
  .pipe(typedoc({
      // TypeScript options (see typescript docs) 
      module: 'commonjs',
      target: 'es5',
      includeDeclarations: true,

      // Output options (see typedoc docs) 
      out: typedoc_dest,
      json: typedoc_dest + 'file.json',

      // TypeDoc options (see typedoc docs) 
      name: 'TypeDoc',
      theme: 'default',
      readme: './README.md',
      ignoreCompilerErrors: false,
      version: true
    })
  );
});