'use strict';

var gulp        = require('gulp');
var config      = require('../configfile.js');
var paths       = require('../../package.json').paths;
var jsdoc       = require('gulp-jsdoc3');

// use external path defined in package.json when --external flag is given
var js_src = config.external ? paths.js.src_ext : paths.js.src;

/**
 * By default, documentation is output to docs/gen. gulp-jsdoc3 does not modify the source vinyl stream so the output location can only be specified via config, not gulp.dest().
 */
// var js_dest = config.external ? paths.js.dest_ext : paths.js.dest;

/**
 * JSDoc 3 is an API documentation generator for JavaScript, similar to Javadoc or phpDocumentor. You add documentation comments directly to your source code, right alongside the code itself. The JSDoc tool will scan your source code and generate an HTML documentation website for you.
 */

gulp.task('jsdoc', function(cb){
  var config = require('../../jsdocConfig');

  gulp.src(['README.md', js_src + '**/*.js'], {read: false})
    .pipe(jsdoc(config, cb));
});
