'use strict';

var gulp      = require('gulp');
var pump      = require('pump');
var paths     = require('../../package.json').paths;
var uglify    = require('gulp-uglify');
var config    = require('../configfile.js');

// use external path defined in package.json when --external flag is given
var ts_dest = config.external ? paths.ts.dest_ext : paths.ts.dest;

gulp.task('js-compress', function(cb){
  pump([
    gulp.src(ts_dest+'/bundle.js'),
    uglify(),
    gulp.dest(ts_dest)
  ],
  cb
  );
});
